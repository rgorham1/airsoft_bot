from selenium import webdriver
from time import sleep


class Airsoft:
    def __init__(self):
        print('Airsoft script started')
        # self.main_page = 'http://www.airsoftgi.com/'
        self.browser = None

    def start_browser(self):
        """Method that initiates the browser and implicit wait time"""
        self.browser = webdriver.Chrome()
        # self.browser.implicitly_wait(5)

    def goto_page(self, page):
        self.browser.get(page)

    def find_gun_deals(self, which_gun):
        try:
            self.browser.get('http://www.airsoftgi.com/penny-deal.php')
            deals = self.browser.find_elements_by_class_name('inlineBlock')
            penny_deals = deals[5:7]
            if which_gun == 1:
                link = penny_deals[0].find_element_by_tag_name('a')
                link.click()
                check = self.browser.find_element_by_css_selector('#products_info_details > table > tbody > '
                                                                  'tr:nth-child(3) > td > table > tbody > '
                                                                  'tr:nth-child(3) > td > table:nth-child(1) > tbody > '
                                                                  'tr:nth-child(2) > td:nth-child(1)')
                print(check.text)
            else:
                link = penny_deals[1].find_element_by_tag_name('a')
                link.click()
        except Exception, e:
            print(str(e))


airsoft = Airsoft()
airsoft.start_browser()
airsoft.find_gun_deals(1)

